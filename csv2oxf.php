#!/usr/bin/env php
<?php

include 'config.php';
include 'parser.php';

while(TRUE == TRUE) {
	foreach (new DirectoryIterator($in) as $fileInfo) {
		if($fileInfo->isDot()) continue;
		$file = $fileInfo->getFilename();
		$transactions = parseCSV($in.'/'.$file);
		$output = ofxBody($transactions);
		file_put_contents($out.'/'.$file.'-'.time().'.ofx', $output);
		unlink($in.'/'.$file);
		//chmod($in.'/'.$file, 0777);
	}
	sleep(60);
}