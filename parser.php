<?php

function parseCSV($file)
{
	$delimiter = ';';
	$rawcsv  = explode("\n", file_get_contents($file));
	$columns = str_getcsv(array_shift($rawcsv),$delimiter);
	foreach ($rawcsv as $row) {
        	if (!empty($row)) {	
			$rows[] = str_getcsv($row,$delimiter);
		}
	}
	return processCSV($columns,$rows);
}

function processCSV($columns, $rows)
{
	$columns = array_map('strtolower', $columns);
	foreach ($rows as $transaction) {
		$transactions[] = array_combine($columns, $transaction);
	}
	return $transactions;
}

function ofxHeader()
{
	$dateserver = date('Ymd');
	$type = "CHECKING";
	$bankid = "1337";
	$accid = "1337"; 
	$output = "
OFXHEADER:100
DATA:OFXSGML
VERSION:102
SECURITY:NONE
ENCODING:USASCII
CHARSET:1252
COMPRESSION:NONE
OLDFILEUID:NONE
NEWFILEUID:NONE

<OFX>
    <SIGNONMSGSRSV1>
        <SONRS>
            <STATUS>
                <CODE>0</CODE>
                <SEVERITY>INFO</SEVERITY>
            </STATUS>
                  <DTSERVER>$dateserver</DTSERVER>
                  <LANGUAGE></LANGUAGE>
        </SONRS>
    </SIGNONMSGSRSV1>
    <BANKMSGSRSV1>
        <STMTTRNRS>
            <TRNUID>1</TRNUID>
            <STATUS>
                <CODE>0</CODE>
                <SEVERITY>INFO</SEVERITY>
            </STATUS>
            <STMTRS>
            <CURDEF>ZWD</CURDEF>
            <BANKACCTFROM>
                <BANKID>$bankid</BANKID>
                <ACCTID>$accid</ACCTID>
                <ACCTTYPE>$type</ACCTTYPE>
            </BANKACCTFROM>
            
            <BANKTRANLIST>
            <DTSTART>19700101</DTSTART>
            <DTEND>$dateserver</DTEND>";
	return $output;
}

function ofxFooter($date,$balance)
{
	$output = "
            </BANKTRANLIST>
            
            <LEDGERBAL>
                <BALAMT>$balance</BALAMT>
                <DTASOF>$date</DTASOF>
            </LEDGERBAL>
            </STMTRS>
        </STMTTRNRS>
    </BANKMSGSRSV1>
</OFX>";
	return $output;

}

function ofxBody($transactions)
{
	$limit=49;
	$count=0;
	$fdate = "";
	$balance = "";
	$ofxdata = ofxHeader();
	foreach ($transactions as $transaction) {
		$date = str_replace('/', '-', $transaction['datum']);
		$date = date('Ymd', strtotime($date)); 
		$amount = $transaction['bedrag'];
		$idparts = $transaction['datum'].$transaction['bedrag'].$transaction['rekeningnummer']; 
		$id = substr( hexdec( md5($idparts) ) ,3,11); 
		$payee = $transaction['beschrijving']; 
		$memo = $transaction['mededeling'];
		$checkid = $transaction['uittrekselnummer'];

		if($fdate == "") { $fdate = $date; }
		if($balance == "") { $balance = $transaction['rekeningsaldo']; }

		$ofxdata .= "
                <STMTTRN>
                    <TRNTYPE>OTHER</TRNTYPE>
                    <DTPOSTED>$date</DTPOSTED>
                    <TRNAMT>$amount</TRNAMT>
                    <FITID>$id</FITID>
                    <NAME>$payee</NAME>
                    <MEMO>$memo</MEMO>
                    <CHECKNUM>$checkid</CHECKNUM>
                </STMTTRN>";
		if($limit<>0) {
			$count++;
			if($count>$limit) {
				break;
			}
		}
        }
	$ofxdata .= ofxFooter($fdate,$balance);
	return $ofxdata;
}
